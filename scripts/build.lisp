(in-package #:cl-user)

(ql:quickload :uncommon)

(save-lisp-and-die (format nil "uncommon_~a" (get-universal-time))
		   :executable t
		   :toplevel #'uncommon-run::main)
