(in-package #:uncommon-repl)

(defclass repl (uncommon-editor:buffer)
  ((history :initarg :keywords
	    :accessor repl-history
	    :type list)
   (last-output :initarg :prompt
		:accessor repl-last-output
		:type repl-output)
   (prompt :initarg :prompt
	   :accessor repl-prompt
	   :type string)
   (font :initarg :font
	 :accessor repl-font
	 :type string)))

(defvar *current-repl* nil)

(defvar *repl-theme* nil)


(defstruct repl-output position content)

(defun make-repl (&key id (type "scintilla")
		       attributes
		       language theme
		       configuration
		       command-table)
  (make-instance 'repl
		 :id id
		 :configuration configuration
		 :command-table command-table
		 :window (actor:make-element
			  :id id
			  :type type 
			  :attributes attributes)
		 :language language
		 :theme theme
		 :file nil
		 :tab "0"
		 :prompt (uncommon-editor::prompt language)))

(defgeneric load-repl (repl language))

;;TODO: Send the history to the REPL history
(defgeneric send-input (language input))

(defgeneric prompt (language))
