(in-package #:uncommon-editor)

;;TODO: Make a sub type of integer
(defstruct rgb
  (red 0 :type integer)
  (green 0 :type integer)
  (blue 0 :type integer))

(defmethod rgb-to-string ((rgb rgb))
  (format nil "~a ~a ~a"
	  (rgb-red rgb)
	  (rgb-green rgb)
	  (rgb-blue rgb)))

(defclass theme ()
  ((id :initarg :id
       :accessor theme-id :type string)
   (bgcolor :initarg :bgcolor
	    :accessor theme-bgcolor
	    :type rgb)
   (fgcolor :initarg :fgcolor
	    :accessor theme-fgcolor
	    :type rgb)
   (comments :initarg :comments 
	     :accessor theme-comments
	     :type rgb)
   (numbers :initarg :numbers
	    :accessor theme-numbers
	    :type rgb)
   (keywords :initarg :keywords
	     :accessor theme-keywords
	     :type rgb)
   (operators :initarg :operators
	      :accessor theme-operators
	      :type rgb)
   (strings :initarg :strings
	    :accessor theme-strings
	    :type rgb)
   (caret :initarg :caret
	  :accessor theme-caret
	  :type rgb)))


(defun make-theme (&key id bgcolor fgcolor
			comments numbers
			keywords operators
			strings caret)
  (make-instance 'theme
		 :id id
		 :bgcolor bgcolor
		 :fgcolor fgcolor
		 :comments (or comments (make-rgb))
		 :numbers (or numbers (make-rgb))
		 :keywords (or keywords (make-rgb))
		 :operators (or operators (make-rgb))
		 :strings (or strings (make-rgb))
		 :caret (or caret (make-rgb))))

(defgeneric normalize-xml (xml source))

(defgeneric import-theme (source))

(defmethod import-theme ((source pathname)))

(defgeneric export-theme (theme target))

(defmethod export-theme ((theme theme) (target (eql :scintilla)))
  (with-slots (bgcolor fgcolor comments
	       numbers keywords operators
	       strings caret)
      theme 
    `(,(when fgcolor
	 `("FGCOLOR" ,(rgb-to-string fgcolor)))
      ,(when bgcolor
	 `("BGCOLOR" ,(rgb-to-string bgcolor)))
      ,(when comments
	 `("STYLEFGCOLOR1" ,(rgb-to-string comments)))
      ,(when numbers
	 `("STYLEFGCOLOR2" ,(rgb-to-string numbers)))
      ,(when keywords
	 `("STYLEFGCOLOR4" ,(rgb-to-string keywords))) ;; Keywords1
      ,(when operators
	 `("STYLEFGCOLOR10" ,(rgb-to-string operators)))
      ,(when strings
	 `("STYLEFGCOLOR6" ,(rgb-to-string strings))) 
      ,(when caret
	 `("CARETCOLOR" ,(rgb-to-string caret))))))

(defmethod export-theme ((theme theme) (target (eql :xml))))

(defmethod export-theme ((theme theme) (target (eql :json))))

