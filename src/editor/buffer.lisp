(in-package #:uncommon-editor)

(defvar *word-separator* '(#\Space #\( #\Newline))


(defvar *default-buffer-command-table*
  (uncommon-core::make-command-list
   '(("documentation" uncommon-editor::documentation (:control #\k) :prefix)
     ("autocomplete" uncommon-editor::autocomplete (:control #\i) :prefix))))

(defclass buffer ()
  ((id :initarg :id
       :accessor buffer-id
       :type string)
   (window :initarg :window
	   :accessor buffer-window
	   :type actor:handle)
   (language :initarg :language
	     :accessor buffer-language
	     :type language)
   (theme :initarg :theme
	  :accessor buffer-theme)
   (file :initarg :file
	 :accessor buffer-file
	 :type (or pathname nil))
   (command-table :initarg :command-table
		  :accessor buffer-command-table
		  :type key-map)
   (configuration :initarg :configuration
		  :accessor buffer-configuration)
   (tab :initarg :tab
	:accessor buffer-tab)))

(defun make-buffer (&key id (type "scintilla")
			 attributes
			 language theme
			 file tab
			 configuration
			 command-table)
  (make-instance 'buffer
		 :id id
		 :configuration configuration
		 :command-table command-table
		 :window (actor:make-element
			  :id id
			  :type type 
			  :attributes attributes)
		 :language language
		 :theme theme
		 :file file
		 :tab tab))


;; It returns the thing at the caret position
(defgeneric thing-at-caret (buffer type))

(defmethod thing-at-caret ((buffer buffer) (type (eql :word)))
  (labels ((terminate-char-p (char) ; Move this to an external function
	     (or (char= char #\Space)
		 (char= char #\()
		 (char= char #\Newline))))

    (let* ((current-pos (second (get-caret buffer))) ;; lin:col
	   (line-value (actor:get-attribute
			(buffer-window buffer)
			"LINEVALUE"))
	   (line-length (1- (length line-value)))
	   (end-pos (if (= current-pos 0)
			0
			(1- current-pos)))
	   head tail)

      (when (= current-pos 0)
	(return-from thing-at-caret ""))

      (loop for pos from end-pos downto 0
	    for char = (aref line-value pos)
	    when (terminate-char-p char)
	    return (setf head (1+ pos)))

      (unless head (setf head 0))
      (when (> head line-length)
	(setf head (1- head)))
      
      (loop for pos from (1+ end-pos)
	    to line-length
	    for char = (aref line-value pos)
	    when (terminate-char-p char)
	    return (setf tail (1+ pos)))

      (unless tail (setf tail (1+ line-length)))

      (string-trim '(#\Space #\Tab #\Newline)
		   (subseq line-value head tail)))))

(defgeneric absolute-caret (buffer))


;; Returns an absolute position, from current position
(defmethod absolute-caret ((buffer buffer))
  (parse-integer
   (actor:get-attribute (buffer-window buffer) "CARETPOS")))

(defgeneric get-caret (buffer));; Get scintilla CARET

(defmethod get-caret ((buffer buffer))
  (mapcar #'parse-integer
	  (uiop:split-string (actor:get-attribute
			      (buffer-window buffer)
			      "CARET") :separator '(#\, ))))

(defgeneric end-of-line (buffer line))

(defmethod end-of-line ((buffer buffer) (line integer))
  ;;TODO: This sounds like a macro,
  ;; the action of "save z move to x then do y and move back to z"
  (let ((current-pos (get-caret buffer)))
    (move-caret buffer (cons line
			     (second current-pos)))
    (prog1
	(end-of-line buffer :current)
      (move-caret buffer current-pos))))

(defmethod end-of-line ((buffer buffer) (line (eql :current)))
  (1- (length (actor:get-attribute
	       (buffer-window buffer)
	       "LINEVALUE"))))

(defgeneric show-text-box (buffer text &key attributes))

;;TODO: Show an error?
(defmethod show-text-box ((buffer buffer) (text string) &key attributes)
  (let ((caret (get-caret buffer)))
    (and caret 
	 (actor:update-item
	  (buffer-window buffer)
	  (or attributes
	      `(("ANNOTATIONVISIBLE" "BOXED") ; Type?
		(,(format nil "ANNOTATIONTEXT~a" (first caret)) ,text)))))))


(defmethod documentation ((language (eql :show)) (symbol string))
  (unless (uiop:emptyp symbol)
    (let ((current-buffer (editor-current-buffer
			   (current-editor))))
      (show-text-box current-buffer symbol))))

(defmethod documentation ((language (eql :show)) symbol)
  ;;TODO: Redirect all the errors to the "status bar"
  (format t "Show an error"));; ERROR

(defmethod language-documentation ((language (eql :show))
				   (symbol string))
  ""
  )

(defun make-autoshow (n word-list)
  (list (format nil "AUTOCSHOW~a" n)
	(format nil "~{~a~^ ~}" word-list)))

(defmethod show-autocomplete ((scintilla actor:handle) (autocomplete-list list)
			      (word-length integer))
  (actor:update-item scintilla
		     `(,(make-autoshow word-length autocomplete-list))))

(defmethod autocomplete ((language (eql :show)) (autocomplete-list list))
  (unless (uiop:emptyp autocomplete-list)
    (let* ((current-buffer (editor-current-buffer (current-editor)))
	   (tac (thing-at-caret current-buffer :word))
	   (word-length (length tac)))
      (show-autocomplete (buffer-window current-buffer) autocomplete-list word-length))))

(defgeneric buffer-display (action string))

(defgeneric load-keymap (buffer map))

(defmethod load-keymap ((buffer buffer) (map uncommon-core::key-map))
  ;; (actor:update-item (buffer-window buffer)
  ;; 		     `(("k_any" "buffer-map")))
  )

(defgeneric command-action (buffer action))

(defmethod command-action ((buffer buffer) (action (eql :prefix)))
  (thing-at-caret buffer :word))

(defgeneric move-caret (buffer position))

(defmethod move-caret ((buffer buffer) (position cons))
  (actor:update-item (buffer-window buffer)
		     `(("CARET"
			,(format nil "~a,~a"
				 (car position)
				 (second position))))))

(defmethod move-caret ((buffer buffer) (position integer)) ; Position in the line
  (let ((current-line (car (get-caret buffer))))
    (actor:update-item (buffer-window buffer)
		       `(("CARET"
			  ,(format nil "~a,~a"
				   current-line
				   position))))))

(defgeneric insert (buffer position data))

(defmethod insert ((buffer buffer)
		   (position cons)
		   (data string))
  ;;TODO: Move to the real position, not only the position on the line
  (actor:update-item (buffer-window buffer)
		     `((,(format nil "INSERT~a" (second position))
			,data))))

(defmethod insert ((buffer buffer)
		   (position (eql :current))
		   (data string))
  (actor:update-item (buffer-window buffer)
		     `(("ADD" ,data))))

(defmethod insert ((buffer buffer)
		   (position integer)
		   (data string))
  (actor:update-item (buffer-window buffer)
		     `((,(format nil "INSERT~a" position)
			,data))))

(defgeneric buffer-append (buffer position))
(defgeneric buffer-search (buffer word))
(defgeneric search-regex (buffer regex))

(defgeneric load-language (buffer language))

(defgeneric update-configuration (buffer configuration))

(defmethod update-window-configuration ((buffer buffer) (configuration list))
  (actor:update-item (buffer-window buffer) configuration))

(defgeneric apply-theme (buffer))

;;TODO: This only works for an scintilla buffer (due to the split)
(defmethod apply-theme ((buffer buffer))
  (let ((type-dispatch (car (last
			     (uiop:split-string
			      (actor::handle-type
			       (buffer-window buffer))
			      :separator '(#\:))))))
    (actor:update-item (buffer-window buffer)
		       (export-theme (buffer-theme buffer)
				     (read-from-string
				      (format nil ":~a" type-dispatch))))))

(defgeneric get-region (buffer from to))

(defmethod get-region ((buffer buffer) (from integer) to)
  (subseq (actor:get-attribute (buffer-window buffer) "VALUE")
	  from (or to (parse-integer
		       (actor:get-attribute (buffer-window buffer) "COUNT")))))

(defgeneric remove-region (buffer from to))

(defgeneric clear-buffer (buffer))

(defmethod clear-buffer ((buffer buffer))
  (actor:update-item (buffer-window buffer)
		     `(("CLEARALL" "YES"))))
(defgeneric narrow (buffer from to)) ;; Maybe too much Emacs biased

(defgeneric widen (buffer))

(defgeneric get-file-extension (buffer))

(defmethod get-file-extension ((buffer buffer))
  (pathname-type
   (buffer-file buffer)))

(defgeneric save-file (buffer file-name))

(defmethod save-file ((buffer buffer) (file-name pathname))
  ;; (if (uiop:file-exists-p file-name)

  ;;     (with-open-file (f file-name :direction :output
  ;; 				   :if-exists :supersede
  ;; 				   :if-does-not-exist :create)
  ;; 	(write-sequence s f))

  ;;     )

  )



(defgeneric buffer-modified-p (buffer))

(defmethod buffer-modified-p ((buffer buffer))
  (when (string-equal
	 (actor:get-attribute (buffer-window buffer) "MODIFIED") "Yes")
    t))

(defgeneric file-saved-p (buffer))
