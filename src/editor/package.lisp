(cl:in-package #:common-lisp-user)

(defpackage #:uncommon-editor
  (:use :cl)
  (:export
   ;; Callbacks
   #:+default-callbacks+
   #:buffer-map
   #:init-editor
   #:dispatch-key

   ;; Editor
   #:editor
   #:editor-table
   #:editor-configuration
   #:editor-current-buffer
   #:editor-tabs
   #:editor-font

   #:make-editor
   #:current-editor
   #:get-buffer
   #:remove-buffer
   #:save-buffer
   #:new-tab
   #:open-file
   #:insert-tab



   ;; Buffer
   #:*word-separator*
   #:*default-buffer-command-table*

   #:buffer
   #:buffer-id
   #:buffer-window
   #:buffer-language
   #:buffer-theme
   #:buffer-file
   #:buffer-command-table
   #:buffer-configuration
   #:buffer-tab

   #:make-buffer
   #:thing-at-caret
   #:get-caret
   #:end-of-line
   #:move-caret
   #:insert
   #:buffer-append
   #:buffer-search
   #:search-regex
   #:update-window-configuration
   #:get-region
   #:remove-region

   #:show-text-box
   #:buffer-display

   #:load-keymap
   #:command-action

   ;; Language
   #:*default-language-command-table*
   #:language
   #:language-id
   #:language-lexer
   #:language-keywords
   #:language-command-table
   #:language-extensions

   #:autocomplete
   #:editor-apropos
   #:documentation
   #:inline-doc
   #:references
   #:goto-definition
   #:editor-inspect
   #:concrete-tree
   #:editor-eval
   #:editor-compile
   #:editor-compile-file))

