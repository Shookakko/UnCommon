(cl:in-package #:asdf-user)

(defsystem #:uncommon-editor
  :depends-on (#:uncommon-core #:slynk #:slynk-client #:iup-scintilla)
  :serial t
  :components
  ((:file "package")
   (:file "themes")
   (:file "buffer")
   (:file "language")
   (:file "editor")
   (:module languages
    :components
    ((:file "default")
     (:module common-lisp
      :components
      ((:file "common-lisp")
       (:file "hyperspec")))))))


(defsystem #:uncommon-editor.tests
  :author ""
  :license "GNU GPLv3"
  :depends-on (:uncommon-editor :fiveam)
  :components ((:module tests
		:serial t
		:components
		((:file "package")
		 (:file "uncommon-editor-test"))))
  :description "Test system for actor")
