(in-package #:uncommon-editor)


;; slynk-client::*open-connections*

(defvar *common-lisp-port* 2345)

;;TODO: Take the keywords from the connection ?
(defvar *common-lisp-keywords*
  (with-output-to-string (s)
    (format s " ")
    (do-external-symbols (sy :common-lisp) 
      (format s "~a " (string-downcase sy)))
    (format s " ")))

(defvar *language-common-lisp*
  (make-language :id :common-lisp
		 :lexer "lisp"
		 :keywords *common-lisp-keywords*))

(defvar *current-connection* nil)

(defun slynk-server ()
  (unless (find 2345 slynk::*servers* :key #'second :test #'=)
    (slynk:create-server :port *common-lisp-port* :dont-close t)))

(defun default-connection ()
  (unless (and *current-connection*
	       (string-equal
		(slynk-client::host-name *current-connection*) "localhost"))
    (setf *current-connection* (slynk-client:slime-connect "localhost" *common-lisp-port*))))

(defun send-expression (expression &key (connection *current-connection*))
  (slynk-client:slime-eval `(eval (read-from-string ,expression)) connection))

(defmethod prompt ((language (eql :common-lisp)))
  (format nil "~a>"
	  (send-expression "(package-name cl-user::*package*)")))

;; (defmethod load-repl ((repl uncommon-repl::repl) (language (eql :common-lisp)))

;;   )

;;TODO: If there is no current-connection, send a warning in the message bottom line
(defmethod load-language ((buffer buffer) (language (eql :common-lisp)))
  (default-connection)
  (load-symbols)
  (setf (buffer-language buffer) *language-common-lisp*)
  (update-window-configuration buffer
			       `(("LEXERLANGUAGE" ,(language-lexer *language-common-lisp*))
				 ("KEYWORDS1" ',(language-keywords *language-common-lisp*)))))

;; Returns a list with all of it's elements as strings
(defmethod autocomplete ((language (eql :common-lisp)) (prefix string))
  (send-expression (format nil "(car (slynk::simple-completions \"~a\" *package*))"
			   prefix)))

;; Returns a list
(defmethod editor-apropos ((language (eql :common-lisp)) (prefix string))
  (send-expression
      (format nil "(mapcar #'caadr 
(slynk-apropos:apropos-list-for-emacs \"~a\" nil nil ))"
	      prefix)))

;; Returns a string
(defmethod documentation ((language (eql :common-lisp)) (symbol string))
  (send-expression
   (format nil "(slynk:documentation-symbol \"~a\")" symbol)))

(defmethod language-documentation ((language (eql :common-lisp)) (symbol string))
  (let ((definition (lookup symbol)))
    (if (string=
	 (car (last (uiop:split-string definition :separator '(#\/)))) "NIL")
	""
	(prog1 symbol
	  (uiop:launch-program (format nil "open ~a" definition))))))

(defmethod inline-doc ((language (eql :common-lisp)) (symbol string))
  (send-expression
   (format nil "(slynk-backend:arglist '~a)" symbol)))

(defmethod references ((language (eql :common-lisp)) (symbol string))
  (send-expression
   (format nil "(slynk::xref-doit :references '~a)" symbol)))

;;TODO: Ask for the current package
(defmethod prompt ((language (eql :common-lisp))) ">")

(defmethod bformat ((language (eql :common-lisp)))
  (buffer-window (current-buffer))

  )
