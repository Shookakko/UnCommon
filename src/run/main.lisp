(in-package #:uncommon-run)


(defparameter *default-template*
  " <hbox>
  <split orientation=\"horizontal\"
	 showgrip=\"lines\" >
    <vbox>
      <button title=\"Help buttons\" action=\"get-scin-properties\"/>
    </vbox>
    <vbox>
      <hbox>
	<sbox direction=\"east\"
	      showgrip=\"lines\">
	  <tabs>
	    <button title=\"Directory tree\"
		    expand=\"yes\" />
	  </tabs>
	</sbox>
        <hbox id=\"editor-box\"> </hbox>

	<sbox direction=\"west\"
	      showgrip=\"lines\" >
	  <split orientation=\"horizontal\"
		 showgrip=\"lines\" >
	    <button title=\"File information\"
		    expand=\"yes\"/>
	    <button title=\"Easy navigation\"
		    expand=\"yes\"/>
	  </split>
	</sbox>

      </hbox>

      <sbox direction=\"north\"
	    showgrip=\"lines\"
	    minsize=\"0x200\" >
	<tabs id=\"tab-repl\">
	</tabs>
      </sbox>

    </vbox>
  </split>

</hbox>

")


;;TODO: Add menu
(defun main ()
  (actor:empty-virtual-dom)

  (setf uncommon-editor::*editor* (uncommon-editor::simple-editor))

  (let* ((editor-tabs (uncommon-editor::editor-tabs uncommon-editor::*editor*))
	 (repl (uncommon-repl::make-repl
		:id "main-repl"
		:theme uncommon-editor::*dracula-theme*
		:language :common-lisp
		:attributes
		'(("bgcolor" "0 0 0")
		  ("expand" "YES")
		  ("minsize" "100x140")))))
    (sb-int:with-float-traps-masked
	(:divide-by-zero :invalid)
      (iup:with-iup ()
	(iup-scintilla:open)
	(iup:show
	 (iup:dialog
	  (prog1 
	      (eval (actor:handle-to-iup
		     (actor:map-dom-element 
		      (actor:parse-xml-list
		       (xmls:parse *default-template*)))))
	    (actor:insert-item
	     (gethash "editor-box" actor:*virtual-dom*)
	     editor-tabs)
	    (uncommon-editor:insert-tab
	     (gethash "tab-repl" actor:*virtual-dom*)
	     repl)
	    (uncommon-editor::load-language repl :common-lisp)
	    (uncommon-editor::apply-theme repl)
	    )))
	(iup:main-loop)))))



