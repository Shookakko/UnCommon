(in-package #:uncommon-core)

;;TODO: Remove this, this kind of dispatch doesn't work as expected
(alexandria:define-constant +key-prefix+ "K_"
  :test #'string=)

(alexandria:define-constant +key-any+ "K_ANY"
  :test #'string=)

(alexandria:define-constant +arguments+ '(:prefix :buffer :line :region :symbol)
  :test #'equal)

(alexandria:define-constant +control-combinations+
    '((536870989 . #\m) (536870990 . #\n) (536870978 . #\b) (536870998 . #\v)
      (536870979 . #\c) (536871000 . #\x) (536871002 . #\z) (536870988 . #\l)
      (536870987 . #\k) (536870986 . #\j) (536870984 . #\h) (536870983 . #\g)
      (536870982 . #\f) (536870980 . #\d) (536870995 . #\s) (536870977 . #\a)
      (536870992 . #\p) (536870991 . #\o) (536870985 . #\i) (536870997 . #\u)
      (536871001 . #\y) (536870996 . #\t) (536870994 . #\r) (536870981 . #\e)
      (536870999 . #\w) (536870993 . #\q))
  :test #'equal)

(defstruct key
  (modifier :control :type (or keyword nil))
  (char nil  :type character))

(defclass key-map ()
((commands :initarg :commands
	   :accessor key-map-commands
	   :type hash-table)))

(defclass command ()
  ((name  :initarg :name
	  :accessor command-name)
   (key :initarg :key
	:accessor command-key
	:type key)
   (fn :initarg :fn
       :accessor command-fn
       :type (or function null))
   (argument
    :initarg :display-method
    :accessor command-argument)))


(defun define-command (name key function display-method)
  (make-instance 'command :fn function
			  :name name
			  :key key
			  :display-method display-method))

(defun integer-to-modifier (raw-modifier)
  (cond 
    ((or (= iup:+k_rctrl+ raw-modifier)
	 (= iup:+k_lctrl+ raw-modifier)) :control)

    ((or (= iup:+k_rshift+ raw-modifier)
	 (= iup:+k_lshift+ raw-modifier)) :shift)
    (t nil)))

(defgeneric export-key (key))

;;TODO: Add more modifiers
(defmethod export-key ((key key))
  (with-output-to-string (s)
    (format s "~a~a~a"
	    +key-prefix+
	    (case (key-modifier key)
	      (:control "c")
	      (:shift "s"))
	    (key-char key ))))

(defgeneric make-key-map (commands))

(defmethod make-key-map (commands)
  (make-instance 'key-map :commands commands))

(defgeneric get-command (key-map raw-key modifier))

(defmethod get-command ((key-map hash-table)
			(raw-key integer)
			modifier)
  (let ((hash-values
	  (alexandria:hash-table-values key-map)))

    (loop for command in hash-values
	  for key = (command-key command)
	  for char = (key-char key)
	  for key-modifier = (key-modifier key)
	  when (and (= raw-key (char-code char))
		    (equal modifier key-modifier))
	  return command)))

(defmethod get-command ((key-map hash-table)
			(key-char character)
			modifier)
  (let ((hash-values
	  (alexandria:hash-table-values key-map)))

    (loop for command in hash-values
	  for key = (command-key command)
	  for char = (key-char key)
	  for key-modifier = (key-modifier key)
	  when (and (char= key-char char)
		    (equal modifier key-modifier))
	  return command)))

(defgeneric insert-command (key-map key))

(defmethod insert-command ((map key-map) (command command))
  (alexandria:ensure-gethash (command-name command)
			     (key-map-commands map) command))

(defgeneric export-key-map (key-map))

(defmethod export-key-map ((key-map key-map))
  (loop for v being the hash-value of (key-map-commands key-map)
	collect `(,(export-key (command-key v))
		  ,(symbol-name (command-fn v)))))

;; (("name" #'function (:modifier #\a)))
(defun make-command-list (commands)
  (let ((k (make-key-map (make-hash-table :test #'equal))))
    (loop for (name command (modifier character) argument)
	  in commands
	  do (insert-command
	      k (define-command name (make-key :modifier modifier :char character)
		  command argument)))
    k))



