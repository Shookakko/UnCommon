(in-package #:uncommon-core)

(defgeneric file-dialog (message attributes))

(defmethod file-dialog ((message string) (attributes list))
  (let ((dialog (actor:make-element 
		 :id (uuid:make-v1-uuid)
		 :type "file-dialog"
		 :attributes attributes))
	file)
    (actor:map-dom-element dialog)
    (unwind-protect
         (progn
           (iup:popup (eval (actor:handle-to-iup dialog))
		      iup:+center+
		      iup:+center+)
	   (setf file (actor:get-attribute dialog "VALUE")))
      (actor::delete-item dialog))
    file))


(defgeneric input-dialog (prompt))

(defgeneric simple-autocomplete-dialog (prefix list))

(defgeneric message-dialog (type title message buttons)) ; IupMessageDlg

(defgeneric alarm-dialog (dialog message first second third)); IupAlarm, basically a handy yes or no dialog

;;TODO: Create some generic and methods also form IupProgressDlg




