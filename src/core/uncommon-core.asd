(cl:in-package #:asdf-user)

(defsystem #:uncommon-core
  :depends-on (#:actor #:alexandria #:uiop #:chameleon)
  :serial t
  :components
  ((:file "package")
   (:file "commands")
   (:module util
    :components
    ((:file "dialog")
     (:file "various")))
   (:file "config")))

